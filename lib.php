<?php

/**
 * Resource SMS lib file
 *
 * @package    local
 * @subpackage resource_notification
 * @author  Amaya Perera 
 * 
 */

/**
 * Function for handling mod_created events
 * 
 * @param stdClass $eventdata
 * @return bool true
 */
function new_resource_mod_created($eventdata) {
    return new_resource_course_update($eventdata, 'mod_created');
}

/**
 * Function to send messages to users following events that update courses
 * 
 * @param stdClass $eventdata
 * Structure of $eventdata:
 * $eventdata->modulename
 * $eventdata->name       
 * $eventdata->cmid       
 * $eventdata->courseid   
 * $eventdata->userid 
 * @param string $type the name of the event    
 * @return boolean true
 */
function new_resource_course_update($eventdata, $type) {
    global $CFG, $DB;

    $course = $DB->get_record('course', array('id' => $eventdata->courseid));
    $messagetext = get_string($type, 'local_resource_notification', $eventdata->name);

    $coursename = $course->idnumber ? $course->idnumber : $course->fullname;
    $messagetext .= get_string('incourse', 'local_resource_notification', $coursename);

    // required
    $message = new stdClass();
    $message->component = 'resource_notification';
    $message->modulename = $eventdata->modulename;
    $message->name = 'course_updates';
    $message->userfrom = $DB->get_record('user', array('id' => $eventdata->userid));
    $message->subject = $messagetext;
    $message->fullmessage = $messagetext;
    $message->fullmessageformat = FORMAT_PLAIN;
    $message->fullmessagehtml = $messagetext;
    $message->smallmessage = $messagetext;

    // optional
    $message->notification = 1;
    $message->course = $course;
    $message->contexturl = "$CFG->wwwroot/mod/$eventdata->modulename/view.php?id=$eventdata->cmid";
    $message->contexturlname = $eventdata->name;

    // foreach user that can see this
    $context = $context = context_course::instance($eventdata->courseid);
    $users = get_enrolled_users($context);
    $subscribed_users = $DB->get_records_sql('select * from mdl_resourcesms_subscriptions');

    $sms = $message->modulename . ' ' . $message->subject;

    foreach ($users as $user) {
        foreach ($subscribed_users as $subuser) {

            if ($user->id == $subuser->userid) {
                //write_to_file($subuser->telno . ' ' . $sms);
                send_sms($subuser->telno, $sms, '+94711114843');
            }
        }
    }

    return true;
}

/**
 * Function for testing purposes when SMS gateway is not available 
 * @param string $message
 * 
 */
function write_to_file($message) {
    $fp = fopen("/home/amaya/Desktop/resourceblockplus.txt", "a");


    if ($fp == false) {
        echo 'oh fp is false';
    } else {
        fwrite($fp, $message);
        fclose($fp);
    }
}

/**
 * Function to send SMS through Kannel gateway
 * @param string $in_number 
 * @param string $in_msg The message to be sent
 * @param string $from 
 */
function send_sms($in_number, $in_msg, $from) {

    $url = "/cgi-bin/sendsms?username=kannelUser&password=123&from={$from}&to={$in_number}&text={$in_msg}";
    $url = str_replace(" ", "%20", $url);

    $results = file('http://localhost:13013' . $url);
}

?>
