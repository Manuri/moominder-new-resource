<?php

/**
 * Strings for the local plugin local_resource_notification
 * @package local
 * @subpackage resource_notification
 * @author Amaya Perera 
 * 
 */
$string['incourse'] = ' in {$a}.';
$string['messageprovider:course_updates'] = 'Course updates';
$string['mod_created'] = '{$a} created';
$string['mod_updated'] = '{$a} updated';
$string['pluginname'] = 'New Resource Notifications';
?>
