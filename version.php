<?php

/**
 * Version file
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2013091811;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012112900;        // Requires this Moodle version
$plugin->component = 'local_resource_notification';  // Full name of the plugin (used for diagnostics)

?>
